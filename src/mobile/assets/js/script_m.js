$('document').ready(function(){
  // gnb dropdown
  dropdown();
  
  // table-topper filter buttons
  filterBtn()

  //accordion
  accordionTable();

  // 전체메뉴
  allMenu();

  // 아코디언
  accordion();

  // 나의게시물 토글
  myContentsAccordion();
});

// 드롭다운
function dropdown(){
  $('.toggle-dropdown').click(function(e){
    $(this).siblings('.dropdown').slideToggle();
  });
}

function filterBtn(){
  $('.sort button.btn').click(function(){
    $(this).addClass('on').parents('li').siblings().find('button').removeClass('on');
  })
}

//accordion
function accordionTable(){
  $('.accordion-type table .toggle').click(function(e){
    $(this).toggleClass('on').next('tr.content').children('td').toggle();
  })
}

function allMenu(){
  $('.all-menu-btn').click(function(e){
    e.preventDefault();
    $('.all-menu').css('display', 'flex');
  });
  $('.all-menu .close-btn').click(function(e){
    console.log('gg');
    e.preventDefault();
    $('.all-menu').css('display', 'none');
  })
}

function accordion(){
  $('.accordion-list .toggle').click(function(e){
    e.preventDefault();
    $(this).siblings('.content').slideToggle();
  })
}

function myContentsAccordion(){
  $('.my-contents .toggle').click(function(e){
    $(this).toggleClass('on');
  })
}