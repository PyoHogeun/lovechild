$('document').ready(function(){
  // gnb dropdown
  dropdown();
  
  // table-topper filter buttons
  filterBtn()

  //accordion
  accordionTable();

  //all-menu
  allMenu();
});

// 드롭다운
function dropdown(){
  $('.toggle-dropdown').click(function(e){
    $(this).siblings('.dropdown').slideToggle();
  });
}

function filterBtn(){
  $('.sort button.btn').click(function(){
    $(this).addClass('on').parents('li').siblings().find('button').removeClass('on');
  })
}

//accordion
function accordionTable(){
  $('.accordion-type table .toggle').click(function(e){
    $(this).toggleClass('on').next('tr.content').children('td').toggle();
  })
}

function allMenu(){
  $('.top-nav a').mouseover(function(){
    $('.all-menu').css('height', '500px');
    $('section > .container').mouseenter(function(){
      console.log('섹션');
      $('.all-menu').css('height', '0');
    })
  })
}